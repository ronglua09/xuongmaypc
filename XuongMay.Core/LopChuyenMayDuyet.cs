﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XuongMay.Core
{
    //Lop nay chua cac chuyen may trong kho de duyet
    public class LopChuyenMayDuyet
    {
        LopChuyenMay _head, _tail;

        public LopChuyenMay Tail
        {
            get { return _tail; }
            set { _tail = value; }
        }

        public LopChuyenMay Head
        {
            get { return _head; }
            set { _head = value; }
        }
        public LopChuyenMayDuyet(){
            _head = new LopChuyenMay();
            _tail= new LopChuyenMay();
        }
        public void AddTail(LopChuyenMay cm)
        {
            if (Head.ChuyenMay == "" || Head.ChuyenMay==null)
            {
                Head = Tail = cm;
            }
            else
            {
                Tail.Next = cm;
                Tail = cm;
            }
        }
    }
}
