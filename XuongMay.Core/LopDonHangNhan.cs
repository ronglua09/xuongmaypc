﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XuongMay.Core
{
   public  class LopDonHangNhan
    {
        LopDonHang _DonHang;
        int _ID;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public LopDonHang DonHang
        {
            get { return _DonHang; }
            set { _DonHang = value; }
        }
        LopChuyenMay _ChuyenMayLam;

        public LopChuyenMay ChuyenMayLam
        {
            get { return _ChuyenMayLam; }
            set { _ChuyenMayLam = value; }
        }
    }
}
