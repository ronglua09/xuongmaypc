﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace XuongMay.Core
{
    public class LopXuLiPhanCong
    {
        //Khai Bao Lop Xu Li Phan Du Lieu
        LopXuLiDuLieu XuLi = new LopXuLiDuLieu();

        public void PhanCongDonHang(string strFile)
        {
            //Load Danh sach don hang vaf bang don hang tu file import
            DataTable DonHang = new DataTable();
            DonHang = XuLi.LoadDonHangImport(strFile);
            LopDonHang[] DanhSachDonDang = XuLi.LoadDanhSachDonHang(DonHang);

           

            //Duyet tung don hang trong danh sach
            for (int i = 0; i < DanhSachDonDang.Length; i++)
            {
                //Duyet loai hinh tang ca
                int _LoaiTangCa = 0;
                //Duyet so ngay tre
                int _NgayTre = 0;
                //Load File danh sach chuyen may theo ma san pham
                LopChuyenMay[] DanhSachChuyenMay = XuLi.LoadDanhSachChuyenMay(DanhSachDonDang[i].MaSanPham, DanhSachDonDang[i].SoLuongMay);
                A:
                //Danh sach chuyen may duyet lam don hang i
                LopChuyenMayDuyet DsDuyet = new LopChuyenMayDuyet();
                //So Luong hang da lam
                float SoLuongLam = 0;
                int duyetmay = DanhSachDonDang.Length > i ? DanhSachChuyenMay.Length/2 : DanhSachChuyenMay.Length;
                //duyet lan luot cac chuyen may
                for (int j = 0; j < duyetmay ; j++)
			    {
                    //Thoi Gian 1 don hang khong xac dinh thi giao cho don hang cho to co nang suat cao
                    if (DanhSachDonDang[i].NgayHopDong.ToShortDateString() == "1/1/9999")
                    {
                        DanhSachChuyenMay[i].ThoiGianLam = (int)( DanhSachDonDang[i].SoLuongMay / DanhSachChuyenMay[j].NangSuatMa);
                        DsDuyet.AddTail(DanhSachChuyenMay[j]);
                        break;
                    }
                    //XuLiTang Ca
                    TangCa(_LoaiTangCa, _NgayTre, DanhSachChuyenMay[j].ChuyenMay);
                    //Thoi gian cac to lam
                    int Thoigianlam = int.Parse((DanhSachDonDang[i].NgayHopDong - DanhSachChuyenMay[j].NgaySanSang).Days.ToString()) + _NgayTre ;
                    //Duyet so luong lam dc  cac to

                    if (Thoigianlam <=0)
                    {
                        if (_LoaiTangCa < 4)
                        {
                            _LoaiTangCa++;
                        }
                        else
                        {
                            _NgayTre++;
                        }
                        continue;
                    }
                    DanhSachChuyenMay[j].SoLuongLam = SoLuongChuyenMayLam(DanhSachChuyenMay[j].NangSuatMa, Thoigianlam, _LoaiTangCa);
                    SoLuongLam += DanhSachChuyenMay[j].SoLuongLam;
                    DanhSachChuyenMay[j].ThoiGianLam = Thoigianlam;
                    DanhSachChuyenMay[j].SoNgayTre = _NgayTre;
                    DsDuyet.AddTail(DanhSachChuyenMay[j]);
                    if (SoLuongLam > DanhSachDonDang[i].SoLuongDat)
                    {
                        _DanhSachDuyet(ref DanhSachDonDang[i],ref DsDuyet,_LoaiTangCa);
                        break;
                    }
			    }

                //Kiem tra xem cac chuyen may co thuc hien het don hang k?
                if (SoLuongLam < DanhSachDonDang[i].SoLuongDat -10)
                {
                    if (_LoaiTangCa < 4)
                    {
                        _LoaiTangCa++;
                    }
                    else
                    {
                        _NgayTre++;
                    }
                    goto A;
                }
                //Them don hang vao database
                InsertDonHangNhan(ref DsDuyet,ref DanhSachDonDang[i]);
                
            }
        }

        //Tac vu tang ca()
        private void TangCa(int loaitc, int tre,string _ChuyenMay)
        {
            switch(loaitc){
                case 0:
                case 2: break;
                case 1:
                case 3:  XuLi.TangCa(_ChuyenMay);
                    break;
                default: break;
            }
        }
        //Them don hang vao database
        private void InsertDonHangNhan(ref LopChuyenMayDuyet DsDuyet,ref LopDonHang DonHang)
        {
            while (DsDuyet.Head != null)
            {
                LopDonHangNhan Nhandonhang = new LopDonHangNhan();
                Nhandonhang.DonHang = DonHang;
                Nhandonhang.ChuyenMayLam = DsDuyet.Head;
                XuLi.InsertDonHangNhan(Nhandonhang);
                DsDuyet.Head = DsDuyet.Head.Next;
            }
        }
        private void _DanhSachDuyet(ref LopDonHang DonHang, ref LopChuyenMayDuyet DsDuyet,int LoaiTangCa)
        {
            float _LuongHangCon = DonHang.SoLuongDat;
            LopChuyenMay duyet = new LopChuyenMay();
            duyet = DsDuyet.Head;
            while (duyet != null)
            {
                if (_LuongHangCon <= duyet.SoLuongLam)
                {
                    float SN = duyet.NangSuatMa;
                    if (LoaiTangCa==1 ||LoaiTangCa==3)
                    {
                        SN = SN * (float)1.15;
                    }
                    int q = (int)(_LuongHangCon / SN);
                    if ((_LuongHangCon / SN) - (int)(_LuongHangCon / SN) >= 0.6)
                    {
                        duyet.FullTime = true;
                        q = q + 1;
                    }
                    if (q < 1)
                    {
                        duyet.ThoiGianLam = 1;
                        duyet.ThoiGianXong = 1 + ChuNhat(duyet.NgaySanSang, duyet.NgaySanSang.AddDays(q));
                    }
                    else
                    {
                        duyet.ThoiGianLam = q;
                        duyet.ThoiGianXong = q + ChuNhat(duyet.NgaySanSang, duyet.NgaySanSang.AddDays(q));
                    }
                    duyet.SoLuongLam = _LuongHangCon;                    
                }
                else
                {
                    _LuongHangCon = _LuongHangCon - duyet.SoLuongLam;
                    int q = duyet.ThoiGianLam;
                    duyet.ThoiGianXong = q + ChuNhat(duyet.NgaySanSang, duyet.NgaySanSang.AddDays(q));
                }
                duyet.NgayRaChuyen = duyet.NgaySanSang;
                duyet.NgayVaoChuyen = duyet.NgayRaChuyen.AddDays(-5);
                duyet.NgayCat = duyet.NgayVaoChuyen.AddDays(-1);
                if (DonHang.NgayHopDong <= duyet.NgayRaChuyen.AddDays(duyet.ThoiGianXong).AddDays(2))
                {
                    DonHang.NgayMayXong = DonHang.NgayCuoiCung = duyet.NgayRaChuyen.AddDays(duyet.ThoiGianXong);
                    DonHang.NgayXuatHang = DonHang.NgayMayXong.AddDays(1);
                }
                else
                {
                    DonHang.NgayMayXong = duyet.NgayRaChuyen.AddDays(duyet.ThoiGianXong);
                    DonHang.NgayCuoiCung = DonHang.NgayMayXong.AddDays(1);
                    DonHang.NgayXuatHang=DonHang.NgayMayXong.AddDays(3);
                }
                switch (LoaiTangCa)
                {
                    case 1:
                    case 3:
                        duyet.NgayTangCa = duyet.NgayRaChuyen.ToShortDateString();
                        duyet.SoNgayTangCa = duyet.ThoiGianLam;
                    break;
                    default: break;
                }

                if (!duyet.FullTime)
                {
                    duyet.NgaySanSang = DonHang.NgayMayXong.AddDays(-1);
                }
                duyet = duyet.Next;
            }
        }
        private int ChuNhat(DateTime F, DateTime S)
        {
            int cn = 0;
            int so = int.Parse((S - F).Days.ToString());
            for (int i = 1; i <= so; i++)
            {
                if (F.AddDays(i).DayOfWeek.ToString() == "Sunday")
                    cn++;
            }
            return cn;
        }
        private float SoLuongChuyenMayLam(float NangSuat, int ThoiGian, int loaitc)
        {
            switch (loaitc)
            {
                case 0:
                case 3: return NangSuat * ThoiGian;
                    break;
                case 1:
                case 4:
                    return NangSuat * ThoiGian * (float)1.15; break;
                default: return NangSuat * ThoiGian; break;
            }
        }

       
    }
}
