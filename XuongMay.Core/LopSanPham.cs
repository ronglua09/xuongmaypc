﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XuongMay.Core
{
    public class LopSanPham
    {
        string _maSanPham;

        public string MaSanPham
        {
            get { return _maSanPham; }
            set { _maSanPham = value; }
        }
        string _tenSanPham;

        public string TenSanPham
        {
            get { return _tenSanPham; }
            set { _tenSanPham = value; }
        }

        string _style;

        public string Style
        {
            get { return _style; }
            set { _style = value; }
        }

        float _soLuong;

        public float SoLuong
        {
            get { return _soLuong; }
            set { _soLuong = value; }
        }
    }

}
