﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XuongMay.Core
{
    public class LopChuyenMay
    {
        // Khai báo biến
        //Teen Chuyen May
        string _ChuyenMay;

        public string ChuyenMay
        {
            get { return _ChuyenMay; }
            set { _ChuyenMay = value; }
        }

        //Ngay San Sang

        DateTime _ngaySanSang = new DateTime();

        public DateTime NgaySanSang
        {
            get { return _ngaySanSang; }
            set { _ngaySanSang = value; }
        }
        public bool TrangThai()
        {
            if (NgaySanSang <= DateTime.Parse(DateTime.Now.ToShortDateString()))
                return true;
            else
                return false;
        }

        //Nang Suat
        LopSanPham[] _NangSuat;

        public LopSanPham[] NangSuat
        {
            get { return _NangSuat; }
            set { _NangSuat = value; }
        }

        //Tra ra ket qua nang saut cua chuyen may theo MaSanPham
        public float NangSuatTheoMaSP(string MaSanPham)
        {
            float NS = 0;
            for (int i = 0; i < _NangSuat.Length; i++)
            {
                if (_NangSuat[i].MaSanPham == MaSanPham)
                {
                    return _NangSuat[i].SoLuong;
                }
            }

            return NS;
        }
        //Ngay cat don hang
        DateTime _ngayCat;

        public DateTime NgayCat
        {
            get { return _ngayCat; }
            set { _ngayCat = value; }
        }

        //Ngay ra chuyen
        DateTime _ngayRaChuyen;

        public DateTime NgayRaChuyen
        {
            get { return _ngayRaChuyen; }
            set { _ngayRaChuyen = value; }
        }

        //Ngay vao vao chuyen
        DateTime _ngayVaoChuyen;

        public DateTime NgayVaoChuyen
        {
            get { return _ngayVaoChuyen; }
            set { _ngayVaoChuyen = value; }
        }

       //Thời gian xong theo năng suất
        int _ThoiGianLam;

        public int ThoiGianLam
        {
            get { return _ThoiGianLam; }
            set { _ThoiGianLam = value; }
        }

       //Thời gian xong khi tính cả những ngày nghỉ
        int _thoiGianXong;

        public int ThoiGianXong
        {
            get { return _thoiGianXong; }
            set { _thoiGianXong = value; }
        }

        //Nang suat theo 1 ma san phanm
        float _NangSuatMa;

        public float NangSuatMa
        {
            get { return _NangSuatMa; }
            set { _NangSuatMa = value; }
        }

        float _SoLuongLam = 0;

        public float SoLuongLam
        {
            get { return _SoLuongLam; }
            set { _SoLuongLam = value; }
        }
        string _MaSanPham;

        public string MaSanPham
        {
            get { return _MaSanPham; }
            set { _MaSanPham = value; }
        }
        public LopChuyenMay()
        {
            
        }

        //Ngay Bat dau Tang ca 
        string _ngayTangCa = "Không";

        public string NgayTangCa
        {
            get { return _ngayTangCa; }
            set { _ngayTangCa = value; }
        }

        //So Ngay tang ca
        int _soNgayTangCa = 0;

        public int SoNgayTangCa
        {
            get { return _soNgayTangCa; }
            set { _soNgayTangCa = value; }
        }

        //So Ngay tre don hang
        int _soNgayTre = 0;

        public int SoNgayTre
        {
            get { return _soNgayTre; }
            set { _soNgayTre = value; }
        }


        private bool _Full = false;

        public bool FullTime
        {
            get { return _Full; }
            set { _Full = value; }
        }
        public LopChuyenMay Next = null;
    }
}
