﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace XuongMay.Core
{
    public class LopXuLiDuLieu
    {
        //Bien su dung
        OleDbCommand cmd;
        OleDbConnection con;
        OleDbDataAdapter da;
        OleDbDataAdapter dacm;
        DataTable dt;
        public LopXuLiDuLieu()
        {
        }

        //Xu Li Ket Noi
        public OleDbConnection KetNoi()
        {
            string strcon = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=..\..\data\XuongMay.mdb";
            con = new OleDbConnection(strcon);
            return con;
        }
        public OleDbConnection KetNoi(string strConnect)
        {
            string strcon;
            if (strConnect == "1")
                strcon = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=..\..\data\XuongMay.mdb";
            else
                strcon = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strConnect + ";Extended Properties=" + '"' + "Excel 8.0;HDR=YES" + '"';
            con = new OleDbConnection(strcon);
            return con;
        }

        public void ThucHienXuLi(OleDbConnection _Con, OleDbCommand cmd)
        {
            //Kiem tra ket noi
            if(_Con.State != ConnectionState.Open) _Con.Open();
            //Thuc hien truy van xu li
            cmd.ExecuteNonQuery();
            //Kiem tra ket noi
            if (_Con.State != ConnectionState.Closed) _Con.Close();
        }
        public void ThucHienXuLi(string strInsert)
        {
            KetNoi();
           //Kiem tra ket noi
            if (con.State != ConnectionState.Open)
                con.Open();
            cmd = new OleDbCommand(strInsert, con);
            //Thuc hien truy van xu li
            cmd.ExecuteNonQuery();
            //Kiem tra ket noi
            if (con.State != ConnectionState.Closed)
                con.Close();
        }
        //Load du lieu len tu database
        public DataTable LoadChuyenMay(int loai)
        {
            dt = new DataTable();
            if (loai == 1)
            {
                da = new OleDbDataAdapter("SELECT * FROM CHUYENMAY ORDER BY SANSANG", KetNoi("1"));
                da.Fill(dt);
            }
            else{
                dacm = new OleDbDataAdapter("SELECT ID, TENTO AS [Chuyền May], FORMAT(SANSANG,'dd/mm/yyyy') AS [Ngày Sẵn Sàng] FROM CHUYENMAY ORDER BY SANSANG", KetNoi("1"));
                dacm.Fill(dt);
            }
           
            return dt;
        }
        public DataTable LoadChuyenMay(string loai)
        {
            dt = new DataTable();
            da = new OleDbDataAdapter("SELECT  [CHUYENMAY].TENTO, SANSANG  FROM CHUYENMAY inner join NANGSUAT ON"
                                        + " CHUYENMAY.TENTO = NANGSUAT.TENTO WHERE MASANPHAM ='" + loai + "'  ORDER BY SANSANG", KetNoi("1"));
            da.Fill(dt);
            return dt;
        }

        public DataTable LoadNangSuat(int loai)
        {
            dt = new DataTable();
            if(loai==1)
                da = new OleDbDataAdapter("SELECT * FROM NANGSUAT ORDER BY ID", KetNoi("1"));
            else
                da = new OleDbDataAdapter("SELECT ID, MASANPHAM AS [MÃ HÀNG], TENTO AS [CHUYỀN MAY], SOLUONG as [NĂNG SUẤT] FROM NANGSUAT", KetNoi("1"));
            da.Fill(dt);
            return dt;
        }
        public DataTable LoadSanPham(int loai)
        {
            dt = new DataTable();
            if(loai==1)
                da = new OleDbDataAdapter("SELECT * FROM SANPHAM", KetNoi("1"));
            else
                da = new OleDbDataAdapter("SELECT ID, MASANPHAM AS [MÃ HÀNG], TENSANPHAM AS [TÊN HÀNG] FROM SANPHAM", KetNoi("1"));
            da.Fill(dt);
            return dt;
        }

        public DataTable LoadDonHang(int loai)
        {

            dt = new DataTable();
            if(loai==1)
                da = new OleDbDataAdapter("SELECT * FROM DonHang WHERE SOLUONGMAY IS NOT NULL ORDER BY ID", KetNoi("1"));
            else
                da = new OleDbDataAdapter("SELECT ID, Po,KhachHang as [Khách Hàng], MASANPHAM  as [Mã Hàng],STYLE as[Style(PO)],"
                    +" TenSanPham as [Tên Hàng], Format(NgayHopDong,'dd/mm/yyyy') as [Ngày Hợp Đồng],"
                    + " SoLuongDat as [Số Lượng Đặt], SoLuongMay as [Số Lượng May],"
                    + "  Format(NgayDuyet,'dd/mm/yyyy') as [Ngày Duyệt] FROM DonHang WHERE SOLUONGMAY IS NOT NULL ORDER BY ID", KetNoi("1"));
            da.Fill(dt);
            return dt;
        }

        public DataTable LoadDonHangImport(string strConnect)
        {
            string strcon;
            strcon = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strConnect + ";Extended Properties=" + '"' + "Excel 8.0;HDR=YES" + '"';
            con= new OleDbConnection(strcon);
            da = new OleDbDataAdapter("SELECT * FROM [DonHang$] WHERE SOLUONGMAY IS NOT NULL", con);
            dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public DataTable LoadDonHangNhan()
        {
            DataTable dtp = new DataTable();
            DataTable dtm = new DataTable();

            DataSet ds = new DataSet();
            da = new OleDbDataAdapter("select distinct ChuyenMay from donhangnhan order by Chuyenmay", KetNoi());
            da.Fill(dtm);
            
            for (int i = 0; i < dtm.Rows.Count; i++)
            {
                string T = dtm.Rows[i][0].ToString();
                da = new OleDbDataAdapter(string.Format(@"SELECT ID, Po,KhachHang as [Khách Hàng], MASANPHAM  as [Mã Hàng], STYLE as [Style(PO)],"
                    + " TenSanPham as [Tên Hàng], Format(NgayHopDong,'dd/mm/yyyy') as [Ngày Hợp Đồng],"
                    + " SoLuongDat as [Số Lượng Đặt], SoLuongMay as [Số Lượng May], ChuyenMay as [Chuyền May],"
                    + " FORMAT(NGAYCAT,'DD/MM/YYYY') as [Ngày Cắt],FORMAT(NGAYVAOCHUYEN,'DD/MM/YYYY') as [Ngày Vào Chuyền],"
                    + " FORMAT(NGAYRACHUYEN,'DD/MM/YYYY') as [Ngày Ra Chuyền], NANGSUAT as [Năng Suất],SONGAY as [HT],SONGAYHT as [TG],"
                    + " FORMAT(NGAYMAYXONG,'DD/MM/YYYY') as [Ngày May Xong] ,FORMAT(NGAYCUOI,'DD/MM/YYYY') as [Ngày Cuối Cùng],FORMAT(NGAYXUATDI,'DD/MM/YYYY') as [Ngày Xuất Đi],"
                    + " NgayTangCa as [Ngày tăng ca],SONGAYTangCA as [Số Ngày Tăng Ca], SONGAYTRE as [Số Ngày Trễ] from donhangnhan where chuyenmay='{0}' and SOLUONGMAY IS NOT NULL order by Chuyenmay, ID",
                    dtm.Rows[i][0].ToString()), KetNoi());
                //da = new OleDbDataAdapter("select * from donhangnhan where chuyenmay='"+T+"'", KetNoi());
                da.Fill(dtp);
                dtp.Rows.Add(NewRows(dtp));
            }
            return dtp;
        }
        public DataTable LoadDonHangNhan(int iz)
        {
            DataTable dtp = new DataTable();

            da = new OleDbDataAdapter(string.Format(@"Select * from donhangnhan where SOLUONGMAY IS NOT NULL order by Chuyenmay"),KetNoi());
            da.Fill(dtp);
            return dtp;
        }
        public DataTable LoadDonHangNhan(string ChuyenMay)
        {
            DataTable dtp = new DataTable();
            da = new OleDbDataAdapter(string.Format(@"SELECT ID, Po,KhachHang as [Khách Hàng], MASANPHAM  as [Mã Hàng], STYLE as [Style(PO)],"
                + " TenSanPham as [Tên Hàng], Format(NgayHopDong,'dd/mm/yyyy') as [Ngày Hợp Đồng],"
                + " SoLuongDat as [Số Lượng Đặt], SoLuongMay as [Số Lượng May], ChuyenMay as [Chuyền May],"
                + " FORMAT(NGAYCAT,'DD/MM/YYYY') as [Ngày Cắt],FORMAT(NGAYVAOCHUYEN,'DD/MM/YYYY') as [Ngày Vào Chuyền],"
                + " FORMAT(NGAYRACHUYEN,'DD/MM/YYYY') as [Ngày Ra Chuyền], NANGSUAT as [Năng Suất],SONGAY as [HT],SONGAYHT as [TG],"
                + " FORMAT(NGAYMAYXONG,'DD/MM/YYYY') as [Ngày May Xong] ,FORMAT(NGAYCUOI,'DD/MM/YYYY') as [Ngày Cuối Cùng],FORMAT(NGAYXUATDI,'DD/MM/YYYY') as [Ngày Xuất Đi],"
                + " NgayTangCa as [Ngày tăng ca],SONGAYTangCA as [Số Ngày Tăng Ca], SONGAYTRE as [Số Ngày Trễ] from donhangnhan where chuyenmay='{0}' and SOLUONGMAY IS NOT NULL order by Chuyenmay",
                ChuyenMay), KetNoi());

            da.Fill(dtp);
            return dtp;
        }

        public void TangCa(string _ChuyenMay)
        {
            dt = new DataTable();
            da = new OleDbDataAdapter(string.Format("SELECT * FROM DONHANGNHAN WHERE CHUYENMAY='{0}'", _ChuyenMay), KetNoi());
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["SoNgayTangCa"].ToString() != "0" && dt.Rows[i]["NgayTangCa"].ToString() != "Không") { continue; }
                float _NangSuat = float.Parse(dt.Rows[i]["NangSuat"].ToString());
                int _SoNgay = int.Parse(dt.Rows[i]["SoNgay"].ToString());
                if (i > 0)
                {
                    string aa = dt.Rows[i - 1]["FullTime"].ToString();
                    if (dt.Rows[i-1]["FullTime"].ToString()!="True")
                    {
                        _SoNgay++;
                        dt.Rows[i]["NgayRaChuyen"] = DateTime.Parse(dt.Rows[i - 1]["NgayMayXong"].ToString()).AddDays(-1).ToShortDateString();
                    }
                    else
                    dt.Rows[i]["NgayRaChuyen"] = dt.Rows[i - 1]["NgayMayXong"].ToString();
                }
                if (DateTime.Parse(dt.Rows[i]["NgayRaChuyen"].ToString()) > DateTime.Now)
                {
                    _SoNgay = XuLiNgay(_SoNgay);
                    dt.Rows[i]["SoNgay"] = _SoNgay.ToString();
                    dt.Rows[i]["SoNgayHT"] = _SoNgay + ChuNhat(DateTime.Parse(dt.Rows[i]["NgayRaChuyen"].ToString()), DateTime.Parse(dt.Rows[i]["NgayRaChuyen"].ToString()).AddDays(_SoNgay));
                    dt.Rows[i]["SoNgayTangCa"] = _SoNgay;
                    dt.Rows[i]["NgayMayXong"] = DateTime.Parse(dt.Rows[i]["NgayRaChuyen"].ToString()).AddDays(int.Parse(dt.Rows[i]["SoNgayHT"].ToString())).ToShortDateString();
                    dt.Rows[i]["NgayTangCa"] = dt.Rows[i]["NgayRaChuyen"].ToString();
                }
                else
                {
                    _SoNgay = XuLiNgay(_SoNgay, DateTime.Parse(dt.Rows[i]["NgayRaChuyen"].ToString()));
                    dt.Rows[i]["SoNgay"] = _SoNgay.ToString();
                    dt.Rows[i]["SoNgayHT"] = _SoNgay + ChuNhat(DateTime.Parse(dt.Rows[i]["NgayRaChuyen"].ToString()), DateTime.Parse(dt.Rows[i]["NgayRaChuyen"].ToString()).AddDays(_SoNgay));
                    dt.Rows[i]["NgayMayXong"] = DateTime.Parse(dt.Rows[i]["NgayRaChuyen"].ToString()).AddDays(int.Parse(dt.Rows[i]["SoNgayHT"].ToString())).ToShortDateString();
                    dt.Rows[i]["SoNgayTangCa"] = (DateTime.Parse(dt.Rows[i]["NgayMayXong"].ToString()) - DateTime.Now).Days.ToString() ;
                    dt.Rows[i]["NgayTangCa"] = DateTime.Now.ToShortDateString();
                }

                dt.Rows[i]["NgayVaoChuyen"] = DateTime.Parse(dt.Rows[i]["NgayRaChuyen"].ToString()).AddDays(-5).ToShortDateString();
                dt.Rows[i]["NgayCat"] = DateTime.Parse(dt.Rows[i]["NgayRaChuyen"].ToString()).AddDays(-6).ToShortDateString();               

            }
            UpdateDonHangNhan(dt, _ChuyenMay);
        }
        private int ChuNhat(DateTime F, DateTime S)
        {
            int cn = 0;
            int so = int.Parse((S - F).Days.ToString());
            for (int i = 1; i <= so; i++)
            {
                if (F.AddDays(i).DayOfWeek.ToString() == "Sunday")
                    cn++;
            }
            return cn;
        }
        private int XuLiNgay(int Ngay)
        {
            int t1 = (int)((double)Ngay /1.15);
            double t2 = (double)Ngay /1.15;
            if (t2 - t1 > 0.5)
                return t1 + 1 > 0 ? t1 + 1 : -(t1 + 1);
            return t1 > 0 ? t1: -t1;
        }
        private int XuLiNgay(int Ngay, DateTime time)
        {
            int _tg = int.Parse((DateTime.Now-time).Days.ToString());
            Ngay -= _tg;
            int t1 = (int)((double)Ngay / 1.15);
            double t2 = (double)Ngay / 1.15;
            if (t2 - t1 > 0.5)
                return _tg + t1 + 1 > 0 ? _tg + t1 + 1 : -(_tg + t1 + 1);
            return t1 + _tg > 0 ? t1 + _tg : -(t1 + _tg);
        }
        private DataRow NewRows(DataTable dtp)
        {
            return dtp.NewRow();
        }
        //Insert chen de lieu vao database
        //Them du lieu vao don hang
        public void InsertDonHang(LopDonHang DonHang)
        {
            string strInsert = string.Format(" INSERT INTO DONHANG(ID,PO,KHACHHANG,MASANPHAM,STYLE,TENSANPHAM,NGAYHOPDONG,SOLUONGDAT,SOLUONGMAY,NGAYDUYET) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')",
                NumberRow("DonHang").ToString(), DonHang.PO, DonHang.KhachHang, DonHang.MaSanPham,DonHang.Style, DonHang.TenSanPham,
                DonHang.NgayHopDong.ToShortDateString(), DonHang.SoLuongDat.ToString(), DonHang.SoLuongMay.ToString(), DonHang.NgayDuyet.ToShortDateString());
            ThucHienXuLi(strInsert);
        }
        //them du lieu vao don hang nhan

        public int NumberRow(string Table)
        {
            int index = 0;
            dt = new DataTable();

            da = new OleDbDataAdapter(string.Format("Select MAx(ID) From {0} ",Table), KetNoi());
            da.Fill(dt);
            string tt = dt.Rows[0][0].ToString();
            if(tt=="") tt="1";
            index = int.Parse(tt);
            index++;
            return index;
        }
        public void InsertDonHangNhan(LopDonHangNhan DonHangNhan)
        {
            if (DonHangNhan.DonHang.Style == "")
            {
                DonHangNhan.DonHang.Style = "0";
            }
            if (DonHangNhan.DonHang.MaSanPham=="")
            {
                DonHangNhan.DonHang.MaSanPham = "0";
            }
            string strInsert = string.Format(" INSERT INTO DonHangNhan(ID,PO,KHACHHANG,MASANPHAM,STYLE,TENSANPHAM,NGAYHOPDONG,SOLUONGDAT,SOLUONGMAY,CHUYENMAY,"
                +"NGAYCAT,NGAYVAOCHUYEN,NGAYRACHUYEN,NANGSUAT,SONGAY,SONGAYHT,NGAYMAYXONG,NGAYCUOI,NGAYXUATDI,NGAYTANGCA,SONGAYTANGCA,SONGAYTRE,FULLTIME)"
            
            +"VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}',{21},{22})",
                NumberRow("DonHangNhan").ToString(), DonHangNhan.DonHang.PO, DonHangNhan.DonHang.KhachHang, DonHangNhan.DonHang.MaSanPham,DonHangNhan.DonHang.Style, DonHangNhan.DonHang.TenSanPham,
                DonHangNhan.DonHang.NgayHopDong.ToShortDateString(), 
                DonHangNhan.DonHang.SoLuongDat.ToString(), DonHangNhan.ChuyenMayLam.SoLuongLam.ToString(), DonHangNhan.ChuyenMayLam.ChuyenMay, 
                DonHangNhan.ChuyenMayLam.NgayCat.ToShortDateString(), DonHangNhan.ChuyenMayLam.NgayVaoChuyen.ToShortDateString(),
                DonHangNhan.ChuyenMayLam.NgayRaChuyen.ToShortDateString(), DonHangNhan.ChuyenMayLam.NangSuatMa.ToString(),
                DonHangNhan.ChuyenMayLam.ThoiGianLam.ToString(), DonHangNhan.ChuyenMayLam.ThoiGianXong.ToString(),
                DonHangNhan.DonHang.NgayMayXong.ToShortDateString(), DonHangNhan.DonHang.NgayCuoiCung.ToShortDateString(),
                DonHangNhan.DonHang.NgayXuatHang.ToShortDateString(), DonHangNhan.ChuyenMayLam.NgayTangCa,
                DonHangNhan.ChuyenMayLam.SoNgayTangCa.ToString(),DonHangNhan.ChuyenMayLam.SoNgayTre.ToString(),DonHangNhan.ChuyenMayLam.FullTime);
            ThucHienXuLi(strInsert);
            DonHangNhan.DonHang.NgayDuyet = DateTime.Now;
            InsertDonHang(DonHangNhan.DonHang);
            UpdateChuyenMay(DonHangNhan.ChuyenMayLam.ChuyenMay,DonHangNhan.ChuyenMayLam.NgaySanSang);
        }
        //them du lieu vao chuyen may
        public void InsertChuyenMay(LopChuyenMay ChuyenMay)
        {
            string strInsert = string.Format(" INSERT INTO CHUYENMAY VALUES({0},'{1}','{2}')",
                NumberRow("ChuyenMay").ToString(), ChuyenMay.ChuyenMay, ChuyenMay.NgaySanSang);
            ThucHienXuLi(strInsert);
        }

        //Them du leu vao nang suat
        public void InsertNangSuat(LopChuyenMay ChuyenMay)
        {
            string strInsert = string.Format(" INSERT INTO NANGSUAT VALUES({0},'{1}','{2}',{3})",
                NumberRow("NangSuat").ToString(), ChuyenMay.MaSanPham, ChuyenMay.ChuyenMay, ChuyenMay.NangSuatMa);
            ThucHienXuLi(strInsert);
        }

        //Them du leu vao san pham
        public void InsertSanPham(LopSanPham SanPham)
        {
            string strInsert = string.Format(" INSERT INTO SANPHAM VALUES({0},'{1}','{2}')",
               NumberRow("SanPham").ToString(), SanPham.MaSanPham, SanPham.TenSanPham);
            ThucHienXuLi(strInsert);
        }

        //Cap nhat du lieu vao trong danh sach

        public LopSanPham[] LoadDanhSachSanPham()
        {
            int Length = LoadSanPham(1).Rows.Count;
            LopSanPham[] temp = new LopSanPham[Length];
            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = new LopSanPham();
                temp[i].TenSanPham = LoadSanPham(1).Rows[i]["TenSanPham"].ToString();
                temp[i].MaSanPham = LoadSanPham(1).Rows[i]["MaSanPham"].ToString();
            }
            return temp;
        }
        public LopDonHang[] LoadDanhSachDonHang(string strFile){
            int Length=0;
            DataTable donhang = new DataTable();
            donhang = LoadDonHangImport(strFile);
            
            //Test so hang ton tai
            for (int i = 0; i < donhang.Rows.Count; i++)
            {
                if (donhang.Rows[i]["Soluongdat"].ToString() != "")
                    Length++;
            }
            if (Length == 0) return null;
            LopDonHang[] temp = new LopDonHang[Length];
            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = new LopDonHang();
                temp[i].PO=donhang.Rows[i]["PO"].ToString();
                temp[i].KhachHang=donhang.Rows[i]["KHACHHANG"].ToString();
                temp[i].MaSanPham=donhang.Rows[i]["MASANPHAM"].ToString();
                temp[i].TenSanPham = donhang.Rows[i]["TENSANPHAM"].ToString();
                temp[i].Style=donhang.Rows[i]["Style"].ToString();
                try
                {
                    temp[i].NgayHopDong = DateTime.Parse(donhang.Rows[i]["NGAYHOPDONG"].ToString());
                }
                catch (Exception ex)
                {
                    temp[i].NgayHopDong = DateTime.Parse("1/1/9999");
                }
                try
                {
                    temp[i].SoLuongDat = float.Parse(donhang.Rows[i]["SOLUONGDAT"].ToString());
                    temp[i].SoLuongMay = float.Parse(donhang.Rows[i]["SOLUONGMAY"].ToString());
                }catch(Exception ex){
                    temp[i].SoLuongDat = 0;
                    temp[i].SoLuongMay = 0;
                }
            }
            return temp;
        }
        public LopDonHang[] LoadDanhSachDonHang(DataTable donhang)
        {
            int Length = 0;
            //Test so hang ton tai
            for (int i = 0; i < donhang.Rows.Count; i++)
            {
                if (donhang.Rows[i]["Soluongdat"].ToString() != "")
                    Length++;
            }
            LopDonHang[] temp = new LopDonHang[Length];
            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = new LopDonHang();
                temp[i].PO = donhang.Rows[i]["PO"].ToString();
                temp[i].KhachHang = donhang.Rows[i]["KHACHHANG"].ToString();
                if (temp[i].KhachHang == "") temp[i].KhachHang = "0";
                if (temp[i].PO == "") temp[i].PO = "0";
                temp[i].MaSanPham = donhang.Rows[i]["MASANPHAM"].ToString();
                temp[i].TenSanPham = donhang.Rows[i]["TENSANPHAM"].ToString();
                if (temp[i].TenSanPham == "") temp[i].TenSanPham = "0";
                try
                {
                    temp[i].NgayHopDong = DateTime.Parse(donhang.Rows[i]["NGAYHOPDONG"].ToString());
                }
                catch (Exception ex)
                {
                    
                     temp[i].NgayHopDong = DateTime.Parse("1/1/9999");
                }
                try
                {
                    temp[i].SoLuongDat = float.Parse(donhang.Rows[i]["SOLUONGDAT"].ToString());
                    temp[i].SoLuongMay = float.Parse(donhang.Rows[i]["SOLUONGMAY"].ToString());
                }
                catch (Exception ex)
                {
                    temp[i].SoLuongDat = 0;
                    temp[i].SoLuongMay = 0;
                }
            }
            return temp;
        }
        public LopDonHangNhan[] LoadDanhSachDonHangNhan()
        {
            DataTable temp = new DataTable();
            temp = LoadDonHangNhan(1);
            LopDonHangNhan[] DHNHN = new LopDonHangNhan[temp.Rows.Count];
            for (int i = 0; i < DHNHN.Length; i++)
            {
                DHNHN[i] = XuLiDaTa(temp.Rows[i]);
            }
            return DHNHN;
        }
        public LopDonHangNhan[] LoadDanhSachDonHangNhan(string ChuyenMay)
        {
            DataTable temp = new DataTable();
            temp = LoadDonHangNhan(ChuyenMay);
            LopDonHangNhan[] DHNHN = new LopDonHangNhan[temp.Rows.Count];
            for (int i = 0; i < DHNHN.Length; i++)
            {
                DHNHN[i] = XuLiDaTa(temp.Rows[i]);
            }
            return DHNHN;
        }
        public LopDonHangNhan[] LoadDanhSachDonHangNhan(DataTable temp)
        {
            LopDonHangNhan[] DHNHN = new LopDonHangNhan[temp.Rows.Count];
            for (int i = 0; i < DHNHN.Length; i++)
            {
                DHNHN[i] = XuLiDaTa(temp.Rows[i]);
            }
            return DHNHN;
        }

        public DataTable LoadDonHangNhan(DataTable dtz,string ChuyenMay)
        {
            DataTable temp = new DataTable();
            temp = dtz;
            temp.Clear();
            for (int i = 0; i < dtz.Rows.Count; i++)
            {
                if (dtz.Rows[i]["Chuyền May"].ToString() == ChuyenMay)
                {
                    temp.Rows.Add(dtz.Rows[i]);
                }
            }
            return temp;
        }
        public LopDonHangNhan XuLiDaTa(DataRow temp)
        {
            LopDonHangNhan DHNHN = new LopDonHangNhan();
            DHNHN.ID = int.Parse(temp["ID"].ToString());
            DHNHN.ChuyenMayLam = new LopChuyenMay();
            DHNHN.DonHang = new LopDonHang();
            DHNHN.DonHang.PO = temp["Po"].ToString();
            DHNHN.DonHang.KhachHang = temp["KhachHang"].ToString();
            DHNHN.DonHang.MaSanPham = temp["Masanpham"].ToString();
            DHNHN.DonHang.Style = temp["style"].ToString();
            DHNHN.DonHang.TenSanPham = temp["Tensanpham"].ToString();
            DHNHN.DonHang.NgayHopDong = DateTime.Parse(temp["Ngayhopdong"].ToString());
            try
            {
                DHNHN.DonHang.NgayNPL = DateTime.Parse(temp["NgayNPL"].ToString());
            }
            catch {
                DHNHN.DonHang.NgayNPL = new DateTime();
            }
            DHNHN.DonHang.SoLuongDat = float.Parse(temp["Soluongdat"].ToString());
            DHNHN.DonHang.SoLuongMay = float.Parse(temp["Soluongmay"].ToString());
            DHNHN.ChuyenMayLam.ChuyenMay = temp["Chuyenmay"].ToString();
            DHNHN.ChuyenMayLam.NgayCat = DateTime.Parse(temp["Ngaycat"].ToString());
            DHNHN.ChuyenMayLam.NgayVaoChuyen = DateTime.Parse(temp["ngayvaochuyen"].ToString());
            DHNHN.ChuyenMayLam.NgayRaChuyen = DateTime.Parse(temp["ngayrachuyen"].ToString());
            DHNHN.ChuyenMayLam.NangSuatMa = float.Parse(temp["nangsuat"].ToString());
            DHNHN.ChuyenMayLam.ThoiGianLam = int.Parse(temp["songay"].ToString());
            DHNHN.ChuyenMayLam.ThoiGianXong = int.Parse(temp["songayht"].ToString());
            DHNHN.DonHang.NgayMayXong = DateTime.Parse(temp["ngaymayxong"].ToString());
            DHNHN.DonHang.NgayCuoiCung = DateTime.Parse(temp["ngaycuoi"].ToString());
            DHNHN.DonHang.NgayXuatHang = DateTime.Parse(temp["ngayxuatdi"].ToString());
            DHNHN.ChuyenMayLam.NgayTangCa = temp["ngaytangca"].ToString();
            DHNHN.ChuyenMayLam.SoNgayTangCa = int.Parse(temp["songaytangca"].ToString());
            DHNHN.ChuyenMayLam.SoNgayTre = int.Parse(temp["songaytre"].ToString());
            return DHNHN;
        }
        public LopChuyenMay[] LoadDanhSachChuyenMay()
        {
            DataTable dtchuyenmay = LoadChuyenMay(1);
            int Length = dtchuyenmay.Rows.Count;
            LopChuyenMay[] temp = new LopChuyenMay[Length];
            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = new LopChuyenMay();
                temp[i].ChuyenMay = dtchuyenmay.Rows[i]["TenTo"].ToString();
                try
                {
                    temp[i].NgaySanSang = DateTime.Parse(dtchuyenmay.Rows[i]["SanSang"].ToString())> DateTime.Now ? DateTime.Parse(dtchuyenmay.Rows[i]["SanSang"].ToString()): DateTime.Now;
                }
                catch (Exception ex)
                {
                    temp[i].NgaySanSang = DateTime.Now;
                }
            }
            return temp;
        }
        public LopChuyenMay[] LoadDanhSachChuyenMay(string MaSanPham)
        {
            DataTable dtchuyenmay = LoadChuyenMay(MaSanPham);
            int Length = dtchuyenmay.Rows.Count;
            LopChuyenMay[] temp = new LopChuyenMay[Length];
            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = new LopChuyenMay();
                temp[i].ChuyenMay = dtchuyenmay.Rows[i]["TENTO"].ToString();
                try
                {
                    temp[i].NgaySanSang = DateTime.Parse(dtchuyenmay.Rows[i]["SANSANG"].ToString());
                }
                catch (Exception ex)
                {
                    temp[i].NgaySanSang = DateTime.Now;
                }
            }
            return temp;
        }
        public LopChuyenMay[] LoadDanhSachChuyenMay(string MaSanPham, float SoLuong)
        {
            DataTable dtchuyenmay = LoadChuyenMay(MaSanPham);
            int Length = dtchuyenmay.Rows.Count;
            LopChuyenMay[] temp = new LopChuyenMay[Length];
            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = new LopChuyenMay();
                temp[i].ChuyenMay = dtchuyenmay.Rows[i]["TENTO"].ToString();
                try
                {
                    temp[i].NgaySanSang = DateTime.Parse(dtchuyenmay.Rows[i]["SANSANG"].ToString());
                    if(temp[i].NgaySanSang < DateTime.Now)
                        temp[i].NgaySanSang = DateTime.Now;
                }
                catch (Exception ex)
                {
                    temp[i].NgaySanSang = DateTime.Now;
                }
                temp[i].MaSanPham = MaSanPham;
                temp[i].NangSuatMa = NangSuatChuyenMay(temp[i].ChuyenMay, MaSanPham);
            }
            for (int i = 0; i < temp.Length - 1; i++)
            {
                 for (int j = i + 1; j < temp.Length; j++)
                    {
                        int date1;
                        try
                        {
                            //So ngay hoan thanh cua to thu i la
                            date1 = (int)(SoLuong / temp[i].NangSuatMa);
                        }
                        catch (Exception ex)
                        {
                            date1 = 999999999;
                        }

                        //So ngay hoan thanh cua to thu i la
                        int date2;
                        try
                        {
                            //So ngay hoan thanh cua to thu i la
                            date2 = (int)(SoLuong / temp[j].NangSuatMa);
                        }
                        catch (Exception ex)
                        {
                            date2 = 999999999;
                        }
                        if (temp[j].NgaySanSang.AddDays(date2) < temp[i].NgaySanSang.AddDays(date1))
                        {
                            LopChuyenMay t = new LopChuyenMay();
                            t = temp[i];
                            temp[i] = temp[j];
                            temp[j] = t;
                        }
                    }
            }
            return temp;
        }
        private void HoanVi(LopChuyenMay a, LopChuyenMay b)
        {
            LopChuyenMay c = new LopChuyenMay();
            c = a;
            a = b;
            b = c;
        }
        public LopChuyenMay[] LoadDanhSachNangSuat()
        {
            DataTable dtNangSuat = new DataTable();
            dtNangSuat = LoadNangSuat(1);
            int Length = dtNangSuat.Rows.Count;
            LopChuyenMay[] temp = new LopChuyenMay[Length];
            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = new LopChuyenMay();
                temp[i].MaSanPham = dtNangSuat.Rows[i]["MaSanPham"].ToString();
                temp[i].ChuyenMay = dtNangSuat.Rows[i]["TenTo"].ToString();
                temp[i].NangSuatMa = float.Parse(dtNangSuat.Rows[i]["SoLuong"].ToString());
            }
            return temp;
        }

        public float NangSuatChuyenMay(LopChuyenMay cm)
        {
            float ns = 0;
            dt = new DataTable();
            da = new OleDbDataAdapter(string.Format("SELECT SOLUONG FROM NANGSUAT Where TenTo='{0}' and MASanpham='{1}'",cm.ChuyenMay,cm.MaSanPham), KetNoi("1"));
            da.Fill(dt);
            try
            {
                ns = float.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception ex) { }
            return ns;      
        }
        public float NangSuatChuyenMay(string ChuyenMay,string MaSanPham)
        {
            float ns = 0;
            LopChuyenMay[] temp = new LopChuyenMay[LoadDanhSachNangSuat().Length];
            temp = LoadDanhSachNangSuat();

            for (int i = 0; i < temp.Length; i++)
            {
                if (temp[i].MaSanPham == MaSanPham && temp[i].ChuyenMay == ChuyenMay)
                    return temp[i].NangSuatMa;
            }
            return ns;
        }

        //Update
        public void UpdateDonHangNhan(DataTable dt, string _ChuyenMay)
        {
            da = new OleDbDataAdapter(string.Format("SELECT * FROM DONHANGNHAN WHERE CHUYENMAY='{0}'",_ChuyenMay),KetNoi());
            OleDbCommandBuilder cmd = new OleDbCommandBuilder(da);
            da.Update(dt);
        }

        public void UpdateDonHangNhan(LopDonHangNhan[] DHNN)
        {
            //ID,PO,KHACHHANG,MASANPHAM,STYLE,TENSANPHAM,NGAYHOPDONG,SOLUONGDAT,SOLUONGMAY,CHUYENMAY
            //NGAYCAT,NGAYVAOCHUYEN,NGAYRACHUYEN,NANGSUAT,SONGAY,
            //SONGAYHT,NGAYMAYXONG,NGAYCUOI,NGAYXUATDI,NGAYTANGCA,SONGAYTANGCA,SONGAYTRE
            string strUp = string.Format("UPDATE DONHANGNHAN SET "
                + "PO='{0}',KHACHHANG='{1}',MASANPHAM='{2}',STYLE='{3}',TENSANPHAM='{4}',NGAYHOPDONG='{5}',SOLUONGDAT='{6}',"
                + "SOLUONGMAY='{7}',CHUYENMAY='{8}',NGAYCAT='{9}',NGAYVAOCHUYEN='{10}',NGAYRACHUYEN='{11}',NANGSUAT='{12}',SONGAY='{13}',SONGAYHT='{14}',"
                + "NGAYMAYXONG='{16}',NGAYCUOI='{17}',NGAYXUATDI='{18}',NGAYTANGCA='{19}',SONGAYTANGCA='{20}',SONGAYTRE='{21}'"
                + "WHERE ID= '{21}'"
                );
            ThucHienXuLi(strUp);
        }
        public void UpdateDonHangNhan(DataTable dt, OleDbDataAdapter dat)
        {
            OleDbCommandBuilder cmd = new OleDbCommandBuilder(dat);
            dat.Update(dt);
        }
        public void UpdateChuyenMay(DataTable dt)
        {
            dacm = new OleDbDataAdapter("SELECT ID,TENTO AS [Chuyền May], FORMAT(SANSANG,'dd/mm/yyyy') AS [Ngày Sẵn Sàng] FROM CHUYENMAY ORDER BY SANSANG", KetNoi("1"));   
            OleDbCommandBuilder cmd = new OleDbCommandBuilder(dacm);
            dacm.Update(dt);
        }
        public void UpdateNangSuat(DataTable dt)
        {
            da = new OleDbDataAdapter("SELECT ID, MASANPHAM AS [MÃ HÀNG], TENTO AS [CHUYỀN MAY], SOLUONG as [NĂNG SUẤT] FROM NANGSUAT", KetNoi("1"));
            OleDbCommandBuilder cmd = new OleDbCommandBuilder(da);
            da.Update(dt);
        }
        public void UpdateSanPham(DataTable dt)
        {
            da = new OleDbDataAdapter("SELECT ID, MASANPHAM AS [MÃ HÀNG], TENSANPHAM AS [TÊN HÀNG] FROM SANPHAM", KetNoi("1"));
            OleDbCommandBuilder cmd = new OleDbCommandBuilder(da);
            da.Update(dt);
        }
        public void UpdateChuyenMay(string _ChuyenMay, DateTime SS)
        {
            string strUp = string.Format("UPDATE CHUYENMAY SET SANSANG='{0}' WHERE TENTO='{1}'",SS.ToShortDateString(),_ChuyenMay);
            ThucHienXuLi(strUp);
        }
        private float SoLuongChuyenMayLam(float NangSuat, int ThoiGian, int loaitc)
        {
            switch (loaitc)
            {
                case 0:
                case 3: return NangSuat * ThoiGian;
                    break;
                case 1:
                case 4:
                    return NangSuat * ThoiGian * (float)1.15; break;
                default: return NangSuat * ThoiGian; break;
            }
        }

         public void Test()
        {
            string inset = "INSERT INTO TEST VALUES(1,'','')";

            ThucHienXuLi(inset);
        }
    }
}
