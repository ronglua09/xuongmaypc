﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XuongMay.Core
{
   public class LopDonHang
    {
       //Ma Khach hang
        string _PO;

        public string PO
        {
            get { return _PO; }
            set { _PO = value; }
        }
       //Ten khach hang
        string _khachHang;

        public string KhachHang
        {
            get { return _khachHang; }
            set { _khachHang = value; }
        }
       //Ma hang

        string _maSanPham;

        public string MaSanPham
        {
            get { return _maSanPham; }
            set { _maSanPham = value; }
        }

       //Ten San Pham

        string _tenSanPham;

        public string TenSanPham
        {
            get { return _tenSanPham; }
            set { _tenSanPham = value; }
        }
       //Ngay theo hop dong

        DateTime _ngayHopDong;

        public DateTime NgayHopDong
        {
            get { return _ngayHopDong; }
            set { _ngayHopDong = value; }
        }
       //Ngay NPL du kien
        DateTime _ngayNPL;

        public DateTime NgayNPL
        {
            get { return _ngayNPL; }
            set { _ngayNPL = value; }
        }

       //SoLuong dat may

        float _soLuongDat;

        public float SoLuongDat
        {
            get { return _soLuongDat; }
            set { _soLuongDat = value; }
        }
       //So Luong may

        float _soLuongMay;

        public float SoLuongMay
        {
            get { return _soLuongMay; }
            set { _soLuongMay = value; }
        }
       //
        DateTime _ngayMayXong;

        public DateTime NgayMayXong
        {
            get { return _ngayMayXong; }
            set { _ngayMayXong = value; }
        }

        DateTime _ngayCuoiCung;

        public DateTime NgayCuoiCung
        {
            get { return _ngayCuoiCung; }
            set { _ngayCuoiCung = value; }
        }
        DateTime _ngayXuatHang;

        public DateTime NgayXuatHang
        {
            get { return _ngayXuatHang; }
            set { _ngayXuatHang = value; }
        }
        DateTime _ngayDuyet;

        public DateTime NgayDuyet
        {
            get { return _ngayDuyet; }
            set { _ngayDuyet = value; }
        }
        string _style = "0";

        public string Style
        {
            get { return _style; }
            set { _style = value; }
        }
    }
}
