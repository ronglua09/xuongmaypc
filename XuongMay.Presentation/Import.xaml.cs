﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using XuongMay.Core;
namespace XuongMay.Presentation
{
    /// <summary>
    /// Interaction logic for Import.xaml
    /// </summary>
    public partial class Import : Window
    {
        public Import()
        {
            InitializeComponent();
        }

        public string fileName = "";
        public int loai = 4;

        private void btnFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                txtFile.Text = dlg.FileName;
            }
        }
        LopXuLiDuLieu Xuli = new LopXuLiDuLieu();
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            fileName = "";
            loai = 4;
            this.Close();
        }
        
        private void btnImport_Click(object sender, RoutedEventArgs e)
        {
            fileName = txtFile.Text;
            if (fileName == "")
            {
                MessageBox.Show("Bạn chưa chọn file import");
                btnFile.Focus();
                return;
            }
            OleDbDataAdapter da;
            DataTable dt;
            DataTable dti;
            if (chkDonHang.IsChecked != true && chkDsTo.IsChecked != true)
            {
                MessageBox.Show("Bạn hãy chọn nội dung import");
                chkDonHang.Focus();
                return;
            }
            if (chkDonHang.IsChecked == true)
            {
                //Kiểm tra sự tồn tại bảng đơn hàng
                #region Kiểm tra sự tồn tại bảng đơn hàng
                try
                {
                    dti = new DataTable();
                    //Loading dữ liệu có sẵn lên
                    da = new OleDbDataAdapter("Select * from [DonHang$]", Xuli.KetNoi(fileName));
                    da.Fill(dti);
                }
                catch (Exception ex) {
                    MessageBox.Show("Lỗi: " + ex.Message);
                    return;
                }
                #endregion
            }
                loai = 0;
            
            //Import dữ liệu tổ may vào csdl luôn
            if(chkDsTo.IsChecked == true)
            {
                #region Import dữ liệu tổ may....
                try
                {
                    dt = new DataTable();
                    dti = new DataTable();
                    //Loading dữ liệu có sẵn lên
                    da = new OleDbDataAdapter("Select * from [tomay$]", Xuli.KetNoi(fileName));
                    da.Fill(dti);
                    da = new OleDbDataAdapter("SELECT * FROM CHUYENMAY", Xuli.KetNoi());
                    da.Fill(dt);

                    for (int i = 0; i < dti.Rows.Count; i++)
                    {
			            if (!Check(dt,dti.Rows[i][1].ToString()))
                        {
                            DataRow r = dt.NewRow();
                            r[0] = "10";
                            r[1] = dti.Rows[i][1].ToString();
                            dt.Rows.Add(r);
                        }
                    }
                    OleDbCommandBuilder cmd = new OleDbCommandBuilder(da);
                    da.Update(dt);
                }
                catch (Exception ex) {
                    MessageBox.Show("Lỗi: "+ ex.Message);
                    return;
                }
                #endregion
               
                #region Import dữ liệu sản phẩm....
                // 
                try
                {
                    dt = new DataTable();
                    dti = new DataTable();
                    //Loading dữ liệu có sẵn lên
                    da = new OleDbDataAdapter("Select * from [SanPham$]", Xuli.KetNoi(fileName));
                    da.Fill(dti);
                    da = new OleDbDataAdapter("SELECT * FROM SanPham", Xuli.KetNoi());
                    da.Fill(dt);

                    for (int i = 0; i < dti.Rows.Count; i++)
                    {
                        if (!Check(dt, dti.Rows[i][1].ToString()))
                        {
                            DataRow r = dt.NewRow();
                            r[0] = "10";
                            r[1] = dti.Rows[i][1].ToString();
                            r[2] = dti.Rows[i][2].ToString();
                            dt.Rows.Add(r);
                        }
                    }
                    OleDbCommandBuilder cmd = new OleDbCommandBuilder(da);
                    da.Update(dt);
                }
                catch (Exception ex) { 
                }
                #endregion
                #region Import dữ liệu năng suất tổ may....
                try
                {
                    dt = new DataTable();
                    dti = new DataTable();
                    //Loading dữ liệu có sẵn lên
                    da = new OleDbDataAdapter("Select * from [nangsuat$]", Xuli.KetNoi(fileName));
                    da.Fill(dti);
                    da = new OleDbDataAdapter("SELECT * FROM Nangsuat", Xuli.KetNoi());
                    da.Fill(dt);

                    for (int i = 0; i < dti.Rows.Count; i++)
                    {
                        if (!Check(dt, dti.Rows[i][1].ToString(), dti.Rows[i][2].ToString()))
                        {
                            DataRow r = dt.NewRow();
                            r[0] = "10";
                            r[1] = dti.Rows[i][1].ToString();
                            r[2] = dti.Rows[i][2].ToString();
                            r[3] = dti.Rows[i][3].ToString();
                            dt.Rows.Add(r);
                        }
                        OleDbCommandBuilder cmd = new OleDbCommandBuilder(da);
                        da.Update(dt);
                    }
                    
                }
                catch (Exception ex) { }
                #endregion
            }
            if (chkDonHang.IsChecked != true)
            {
                loai = 10;
            }
            this.Close();
        }
        private bool Check(DataTable dt, string a)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][1].ToString() == a)
                    return true;
            }
            return false;
        }
        private bool Check(DataTable dt, string a, string b)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][1].ToString() == a && dt.Rows[i][2].ToString() == b)
                    return true;
            }
            return false;
        }
        private void chkDsTo_Checked(object sender, RoutedEventArgs e)
        {
            lblLuuLy.Content = "Danh sách tổ sẽ được thay thế danh sách tổ đã tồn tại";
        }

        private void chkDsTo_Unchecked(object sender, RoutedEventArgs e)
        {
            lblLuuLy.Content = "";
        }

    }
}