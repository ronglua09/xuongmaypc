﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Table;
using OfficeOpenXml.Style;
using OfficeOpenXml.Style.XmlAccess;
using System.Data;
using System.Data.OleDb;
using XuongMay.Core;
using System.IO;

namespace XuongMay.Presentation
{
    /// <summary>
    /// Interaction logic for uscDonHang.xaml
    /// </summary>
    public partial class uscDonHang : UserControl
    {
        public uscDonHang()
        {
            InitializeComponent();
        }

        string Loai = "DH";
        public uscDonHang(string _loai)
        {
            InitializeComponent();
            Loai = _loai;
        }

        LopXuLiDuLieu XuLiDuLieu = new LopXuLiDuLieu();
        LopXuLiPhanCong XuLiPhanCong = new LopXuLiPhanCong();
        DataTable dt = new DataTable();
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            dt = new DataTable();
            switch (Loai)
            {
                case "DH": data.DataContext = dt = XuLiDuLieu.LoadDonHang(2); break;
                case "Import": data.DataContext = dt = XuLiDuLieu.LoadDonHangImport(Loai); break;
                default: data.DataContext = dt = XuLiDuLieu.LoadDonHangImport(Loai); break;
            }
        }
        string values = "";
        string values2 = "";
        private void data_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            values2 = ((TextBox)e.EditingElement).Text;

            if (values2 == values)
            {
                MessageBox.Show("éo có đổi gì");
            }
            else
            {
               // MessageBox.Show("Bạn cập nhật từ " + values + " sang " + values2 + " tại hàng " + dh[intdex].DonHang.PO.ToString());

            }
        }
        int intdex;
        LopDonHangNhan[] dh;
        public DataTable Data()
        {
            dt = new DataTable();
            switch (Loai)
            {
                case "DH": dt = XuLiDuLieu.LoadDonHang(2); break;
                case "Import":dt = XuLiDuLieu.LoadDonHangImport(Loai); break;
                default: dt = XuLiDuLieu.LoadDonHangImport(Loai); break;
            }
            return dt;
        }
        private void data_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            //DataGridColumn a = e.Column.;
            values = (e.EditingEventArgs.Source as TextBlock).Text;
            //DataTable test = XuLiDuLieu.LoadDonHangNhan((DataTable)data.DataContext, "Tổ 1");
            dh = new LopDonHangNhan[XuLiDuLieu.LoadDonHangNhan("Tổ 1").Rows.Count];

            dh = XuLiDuLieu.LoadDanhSachDonHangNhan();
            // DataRow dr = new DataRow();
            intdex = e.Row.GetIndex();
            // data.DataContext = XuLiDuLieu.LoadDanhSachDonHangNhan();
        }

        private void data_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
        }
        private void data_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == DataGrid.DeleteCommand)
            {
                if ((MessageBox.Show("Are you sure you want to delete?", "Please confirm.", MessageBoxButton.YesNo) == MessageBoxResult.Yes))
                {
                    //  Delete Selected Row.
                    DataRowView da = ((DataRowView)data.SelectedItem);
                    MessageBox.Show("ID là: " + da.Row["ID"].ToString());
                }

            }
        }
    }
}
