﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using XuongMay.Core;
namespace XuongMay.Presentation
{
    /// <summary>
    /// Interaction logic for ChexkTime.xaml
    /// </summary>
    public partial class ChexkTime : Window
    {
        public ChexkTime()
        {
            InitializeComponent();
        }
        LopXuLiDuLieu Xulidl = new LopXuLiDuLieu();
        LopXuLiPhanCong Xulipc = new LopXuLiPhanCong();
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime test = DateTime.Parse(timer.Text);
            }catch(Exception ex){

            }
            OleDbDataAdapter da;
            DataTable dt = new DataTable();
            dt = Xulidl.LoadChuyenMay(1);
            DataColumn col = new DataColumn("Thời Gian Còn Lại");

            dt.Columns.Add(col);

            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                try
                {
                    TimeSpan tg = DateTime.Parse(timer.Text) - DateTime.Parse(dt.Rows[i][2].ToString());
                    if (int.Parse(tg.Days.ToString()) > 0)
                        dt.Rows[i][dt.Columns.Count - 1] = tg.Days.ToString();
                    else
                        dt.Rows[i][dt.Columns.Count - 1] = "0";
                }
                catch (Exception ex)
                {
                    dt.Rows[i][dt.Columns.Count - 1] = "null";
                }
            }
            dataGrid1.DataContext = dt.DefaultView;
        }
    }
}
