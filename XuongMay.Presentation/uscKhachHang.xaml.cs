﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Table;
using OfficeOpenXml.Style;
using OfficeOpenXml.Style.XmlAccess;
using System.Data;
using System.Data.OleDb;
using XuongMay.Core;
using System.IO;

namespace XuongMay.Presentation
{
    /// <summary>
    /// Interaction logic for uscKhachHang.xaml
    /// </summary>
    public partial class uscKhachHang : UserControl
    {
        public uscKhachHang()
        {
            InitializeComponent();
        }
        string Loai = "";
        public uscKhachHang(string _Loai)
        {
            InitializeComponent();
        }
        LopXuLiDuLieu XuLiDuLieu = new LopXuLiDuLieu();
        LopXuLiPhanCong XuLiPhanCong = new LopXuLiPhanCong();
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            data.DataContext = XuLiDuLieu.LoadDonHangNhan();
        }
        string values = "";
        string values2 = "";
        private void data_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            values2 = ((TextBox)e.EditingElement).Text;

            if (values2 == values)
            {
                MessageBox.Show("éo có đổi gì");
            }
            else
            {
                MessageBox.Show("Bạn cập nhật từ " + values + " sang " + values2 + " tại hàng " + dh[intdex].DonHang.PO.ToString());

            }
        }
        int intdex;
        LopDonHangNhan[] dh;
        public DataTable Data()
        {
            return XuLiDuLieu.LoadDonHangNhan();
        }
        private void data_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            //DataGridColumn a = e.Column.;
            values = (e.EditingEventArgs.Source as TextBlock).Text;
            //DataTable test = XuLiDuLieu.LoadDonHangNhan((DataTable)data.DataContext, "Tổ 1");
            dh = new LopDonHangNhan[XuLiDuLieu.LoadDonHangNhan("Tổ 1").Rows.Count];

            dh = XuLiDuLieu.LoadDanhSachDonHangNhan();
            // DataRow dr = new DataRow();
            intdex = e.Row.GetIndex();
            // data.DataContext = XuLiDuLieu.LoadDanhSachDonHangNhan();
        }

        private void data_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
        }
        private void data_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == DataGrid.DeleteCommand)
            {
                if ((MessageBox.Show("Are you sure you want to delete?", "Please confirm.", MessageBoxButton.YesNo) == MessageBoxResult.Yes))
                {
                    //  Delete Selected Row.
                    DataRowView da = ((DataRowView)data.SelectedItem);
                    MessageBox.Show("ID là: " + da.Row["ID"].ToString());
                }

            }
        }
    }
}
