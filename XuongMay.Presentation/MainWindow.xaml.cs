﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Table;
using OfficeOpenXml.Style;
using OfficeOpenXml.Style.XmlAccess;
using System.Data;
using System.Data.OleDb;
using XuongMay.Core;
using System.IO;
namespace XuongMay.Presentation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        LopXuLiDuLieu XuLiDuLieu = new LopXuLiDuLieu();
        LopXuLiPhanCong XuLiPhanCong = new LopXuLiPhanCong();
        DataTable Data;
        public MainWindow()
        {
            
            InitializeComponent();
        }

        // đường dẫn file import
        string fileName;
        int _Save = 0;
        string savefile;
        //Export dữ liệu ra
        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "PhanCong_";
            dlg.Filter = "Excel|*.xlsx";
            Nullable<bool> a = dlg.ShowDialog();
           
            if (a == true)
            {
                //try
                //{
                    if (File.Exists(dlg.FileName))
                        File.Delete(dlg.FileName);
                    //Tajo file
                    switch (_Save)
                    {
                        case 0: EXPORT_KetQua(dlg.FileName); break;
                        case 1: EXPORT_ChuyenMay(dlg.FileName); break;
                        case 2: EXPORT_SanPham(dlg.FileName); break;
                        case 3: EXPORT_NangSuat(dlg.FileName); break;
                        case 4: EXPORT_DonHang(dlg.FileName); break;
                        default: EXPORT_KetQua(dlg.FileName); break;
                    }
                    //Tao ra 1 package
                //}
                //catch(Exception ex)
                //{
                //    MessageBox.Show("Có phải bạn đang mở file:  " + dlg.FileName + "\n Bạn có thể đóng file đang mở hoặc là chọn tên khác", "Lỗi File");
                //}
                

            }

        }

        private void EXPORT_ChuyenMay(string FileName)
        {
            using (var Excel = new ExcelPackage(new FileInfo(FileName)))
            {
                //Add worksheet
                ExcelWorksheet worksheet = Excel.Workbook.Worksheets.Add("Chuyen May_" + DateTime.Now.ToShortDateString()); //có thể thay kq bằng tên bảng tùy ý

                worksheet.Cells["A2"].LoadFromDataTable(Data, true, TableStyles.None);
                //Set color cho row
                    worksheet.Row(1).Height = 30;
                    worksheet.Row(2).Height = 40;
                    worksheet.Cells["A1:C1"].Merge = true;
                    worksheet.Cells["B1:C1"].AutoFitColumns(12);
                    worksheet.Cells["A1:C1"].Value = "Update_Tháng " + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                    worksheet.Cells["A2:U2"].Style.WrapText = true;
                    worksheet.Cells["A2:C2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    worksheet.Cells["A2:C2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.YellowGreen);
                    worksheet.Cells["A1:C2"].Style.Font.SetFromFont(new System.Drawing.Font("Times New Roman", 10));
                    worksheet.Cells["A1:C2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells["A1:C2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells["A1:C2"].Style.Font.Bold = true;
                    worksheet.Cells["A1:C1"].Style.Font.Size = 12;

                Excel.Save();
            }
            MessageBox.Show(" Đã lưu file thành công!");
        }

        private void EXPORT_NangSuat(string FileName)
        {
            using (var Excel = new ExcelPackage(new FileInfo(FileName)))
            {
                //Add worksheet
                ExcelWorksheet worksheet = Excel.Workbook.Worksheets.Add("Nang Suat_" + DateTime.Now.ToShortDateString()); //có thể thay kq bằng tên bảng tùy ý
                //Loading table
                DataTable table = new DataTable();
                try
                {
         //           table = (DataTable)dataGrid1.DataContext;
                }
                catch
                {
                    DataView dv = new DataView();
            //        dv = (DataView)dataGrid1.DataContext;
                    table = dv.Table;
                }

                worksheet.Cells["A2"].LoadFromDataTable(Data, true, TableStyles.None);
                //Set color cho row
                //Set color cho row
                worksheet.Row(1).Height = 30;
                worksheet.Row(2).Height = 40;
                worksheet.Cells["A1:D1"].Merge = true;
                worksheet.Cells["B1:D1"].AutoFitColumns(12);
                worksheet.Cells["A1:D1"].Value = "Update_Tháng " + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                worksheet.Cells["A2:D2"].Style.WrapText = true;
                worksheet.Cells["A2:D2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["A2:D2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.YellowGreen);
                worksheet.Cells["A1:D2"].Style.Font.SetFromFont(new System.Drawing.Font("Times New Roman", 10));
                worksheet.Cells["A1:D2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["A1:D2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["A1:D2"].Style.Font.Bold = true;
                worksheet.Cells["A1:D1"].Style.Font.Size = 12;

                Excel.Save();
            }
            MessageBox.Show(" Đã lưu file thành công!");
        }
        private void EXPORT_SanPham(string FileName)
        {
            using (var Excel = new ExcelPackage(new FileInfo(FileName)))
            {
                //Add worksheet
                ExcelWorksheet worksheet = Excel.Workbook.Worksheets.Add("San Pham_" + DateTime.Now.ToShortDateString()); //có thể thay kq bằng tên bảng tùy ý
                //Loading table
                DataTable table = new DataTable();
                try
                {
         //           table = (DataTable)dataGrid1.DataContext;
                }
                catch
                {
                    DataView dv = new DataView();
            //        dv = (DataView)dataGrid1.DataContext;
                    table = dv.Table;
                }

                worksheet.Cells["A2"].LoadFromDataTable(Data, true, TableStyles.None);
                //Set color cho row
                worksheet.Row(1).Height = 30;
                worksheet.Row(2).Height = 40;
                worksheet.Cells["A1:C1"].Merge = true;
                worksheet.Cells["B1:C1"].AutoFitColumns(12);
                worksheet.Cells["A1:C1"].Value = "Update_Tháng " + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                worksheet.Cells["A2:U2"].Style.WrapText = true;
                worksheet.Cells["A2:C2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["A2:C2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.YellowGreen);
                worksheet.Cells["A1:C2"].Style.Font.SetFromFont(new System.Drawing.Font("Times New Roman", 10));
                worksheet.Cells["A1:C2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["A1:C2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["A1:C2"].Style.Font.Bold = true;
                worksheet.Cells["A1:C1"].Style.Font.Size = 12;

                Excel.Save();
            }
            MessageBox.Show(" Đã lưu file thành công!");
        }
        private void EXPORT_KetQua(string FileName)
        {
            using (var Excel = new ExcelPackage(new FileInfo(FileName)))
            {
                //Add worksheet
                ExcelWorksheet worksheet = Excel.Workbook.Worksheets.Add("KQ_" + DateTime.Now.ToShortDateString()); //có thể thay kq bằng tên bảng tùy ý
                //Loading table
               
                try
                {
                    worksheet.Cells["A2"].LoadFromDataTable(Data, true, TableStyles.None);
                    //Set color cho row
                    worksheet.Row(1).Height = 30;
                    worksheet.Row(2).Height = 40;
                    worksheet.Cells["D1:E1"].Merge = true;
                    worksheet.Cells["B1:H1"].AutoFitColumns(12);
                    worksheet.Cells["D1:E1"].Value = "Update_Tháng " + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                    worksheet.Cells["I1:M1"].Merge = true;
                    worksheet.Cells["I1:M1"].Value = "TUANDAT PRODUCTION PLAN";
                    worksheet.Cells["A2:V2"].Style.WrapText = true;
                    worksheet.Cells["A2:V2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    worksheet.Cells["A2:V2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.YellowGreen);
                    worksheet.Cells["A1:V2"].Style.Font.SetFromFont(new System.Drawing.Font("Times New Roman", 10));
                    worksheet.Cells["A1:V2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells["A1:V2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells["A1:V2"].Style.Font.Bold = true;
                    worksheet.Cells["A1:V1"].Style.Font.Size = 12;
                    worksheet.Column(5).Width = 25;
                    Excel.Save();
                }
                catch
                {
                    return;
                }
            }
            MessageBox.Show(" Đã lưu file thành công!");
        }
        private void EXPORT_DonHang(string FileName)
        {
            using (var Excel = new ExcelPackage(new FileInfo(FileName)))
            {
                //Add worksheet
                ExcelWorksheet worksheet = Excel.Workbook.Worksheets.Add("Don Hang_" + DateTime.Now.ToShortDateString()); //có thể thay kq bằng tên bảng tùy ý
                //Loading table
                DataTable table = new DataTable();
                try
                {
            //        table = (DataTable)dataGrid1.DataContext;
                }
                catch
                {
                    DataView dv = new DataView();
            //        dv = (DataView)dataGrid1.DataContext;
                    table = dv.Table;
                }

                worksheet.Cells["A2"].LoadFromDataTable(Data, true, TableStyles.None);
                //Set color cho row
                worksheet.Row(1).Height = 30;
                worksheet.Row(2).Height = 40;
                worksheet.Cells["B1:D1"].Merge = true;
                worksheet.Cells["B1:H1"].AutoFitColumns(12);
                worksheet.Cells["B1:D1"].Value = "Update_Tháng " + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                worksheet.Cells["E1:J1"].Merge = true;
                worksheet.Cells["E1:J1"].Value = "TUANDAT PRODUCTION PLAN";
                worksheet.Cells["A2:J2"].Style.WrapText = true;
                worksheet.Cells["A2:J2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["A2:J2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.YellowGreen);
                worksheet.Cells["A1:J2"].Style.Font.SetFromFont(new System.Drawing.Font("Times New Roman", 10));
                worksheet.Cells["A1:J2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["A1:J2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["A1:J2"].Style.Font.Bold = true;
                worksheet.Cells["A1:J1"].Style.Font.Size = 12;
                worksheet.Column(5).Width = 25;
                Excel.Save();
            }
            MessageBox.Show(" Đã lưu file thành công!");
        }
        //Import dữ liệu vào
        string fileN = "1";
        int loai = 1;
        private void btnImport_Click(object sender, RoutedEventArgs e)
        {
            btnsua.Visibility = Visibility.Hidden;
            Import test = new Import();
            test.ShowDialog();
            fileN = test.fileName;
            loai = test.loai;
            if (loai == 4)
            {
                fileN = "1";
                return;
            }
            if (loai == 10)
            {
                fileN = "1";
            }
            if (test.fileName == "")
            {
                fileN = "1";
                MessageBox.Show("Import thất bại");
                return;
            }
            else
            {
                MessageBox.Show("Import thành công");
                phancong = true;
            }

            
        }
        private bool phancong = false;
        //xuất kết quả phân công
        DataTable dt = new DataTable();
        private void btnKetQua_Click(object sender, RoutedEventArgs e)
        {
            btnsua.Visibility = Visibility.Hidden;
            if (phancong)
            {
                phancong = false;
             
            }
            _Save = 0;

        }
        //update thông tin
        public void UpdateTo()
        {
            _Save = 1;
            MessageBoxResult dt = MessageBox.Show("Bạn Có Muốn Sửa Dữ Liệu", "Thông Báo!", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (dt == MessageBoxResult.Yes)
            {
                //DataView dv = new DataView();
                //dv = (DataView)dataGrid1.DataContext;
                DataTable gg = new DataTable();
                try
                {
                    //gg = (DataTable)dataGrid1.DataContext;
                }
                catch (Exception ex0)
                {
                    try
                    {
                        DataView dv = new DataView();
                      //  dv = (DataView)dataGrid1.DataContext;
                        gg = dv.Table;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                XuLiDuLieu.UpdateChuyenMay(gg);
                MessageBox.Show("Đã cập nhật dữ liệu thành công!");
            }
            else
            {

            }

        }
        public void UpdateSanPham()
        {
            _Save = 2;
            MessageBoxResult dt = MessageBox.Show("Bạn Có Muốn Sửa Dữ Liệu", "Thông Báo!", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (dt == MessageBoxResult.Yes)
            {
                //dv = (DataView)dataGrid1.DataContext;
                DataTable gg = new DataTable();
                try
                {
                    //gg = (DataTable)dataGrid1.DataContext;
                }
                catch (Exception ex0)
                {
                    try
                    {
                        DataView dv = new DataView();
                     //   dv = (DataView)dataGrid1.DataContext;
                        gg = dv.Table;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                XuLiDuLieu.UpdateSanPham(gg);
                MessageBox.Show("Đã cập nhật dữ liệu thành công!");
            }
            else
            {

            }
        }
        public void UpdateNS()
        {
            _Save = 3;
            MessageBoxResult dt = MessageBox.Show("Bạn Có Muốn Sửa Dữ Liệu", "Thông Báo!", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (dt == MessageBoxResult.Yes)
            {
                //dv = (DataView)dataGrid1.DataContext;
                DataTable gg = new DataTable();
                try
                {
                    //gg = (DataTable)dataGrid1.DataContext;
                }
                catch (Exception ex0)
                {
                    try
                    {
                        DataView dv = new DataView();
                      //  dv = (DataView)dataGrid1.DataContext;
                        gg = dv.Table;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                XuLiDuLieu.UpdateNangSuat(gg);
                MessageBox.Show("Đã cập nhật dữ liệu thành công!");
            }
            else
            {

            }
        }
        int type = 0;
        private void menu_dsto_Click(object sender, RoutedEventArgs e)
        {
            _Save = 1;
            try
            {
                type = 2;
                btnExport.Visibility = Visibility.Visible;
                btnsua.Visibility = Visibility.Visible;
                lblStatus.Content = "Danh sách chuyền may";
              //  dataGrid1.DataContext = XuLiDuLieu.LoadChuyenMay(2);
                uscChuyenMay kq = new uscChuyenMay("CM");
                sPnlContent.Children.Clear();
                sPnlContent.Children.Add(kq);
                Data = new DataTable();
                Data = kq.Data();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void menu_dssanpham_Click(object sender, RoutedEventArgs e)
        {
            _Save = 2;
            try
            {
                type = 3;
                btnExport.Visibility = Visibility.Visible;
                btnsua.Visibility = Visibility.Visible;
                lblStatus.Content = "Danh sách các sản phẩm";
                //dataGrid1.DataContext = XuLiDuLieu.LoadSanPham(2);
                uscChuyenMay kq = new uscChuyenMay("SP");
                sPnlContent.Children.Clear();
                sPnlContent.Children.Add(kq);
                Data = new DataTable();
                Data = kq.Data();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menu_ns_Click(object sender, RoutedEventArgs e)
        {
            _Save = 3;
            try
            {
                type = 1;
                btnExport.Visibility = Visibility.Visible;
                btnsua.Visibility = Visibility.Visible;
                lblStatus.Content = "Năng suất các chuyền may";
             //   dataGrid1.DataContext = XuLiDuLieu.LoadNangSuat(2);
                uscChuyenMay kq = new uscChuyenMay("NS");
                sPnlContent.Children.Clear();
                sPnlContent.Children.Add(kq);
                Data = new DataTable();
                Data = kq.Data();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnabout_Click(object sender, RoutedEventArgs e)
        {
            btnsua.Visibility = Visibility.Hidden;
            about ab = new about();
            ab.ShowDialog();

        }

        private void btnsua_Click(object sender, RoutedEventArgs e)
        {
            //try{
            //    btnExport.Visibility = Visibility.Visible;
            //    switch (type)
            //    {
            //        case 1: UpdateNS(); break;
            //        case 2: UpdateTo(); break;
            //        case 3: UpdateSanPham(); break;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}

        }

        private void mn_DsDH_Click(object sender, RoutedEventArgs e)
        {
            _Save = 4;
            try{
                btnsua.Visibility = Visibility.Hidden;
              //  dataGrid1.DataContext = XuLiDuLieu.LoadDonHang(2);
                lblStatus.Content = "Danh sách đơn hàng";
                uscDonHang kq = new uscDonHang("DH");
                sPnlContent.Children.Clear();
                sPnlContent.Children.Add(kq);
                Data = new DataTable();
                Data = kq.Data();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mn_DH_Click(object sender, RoutedEventArgs e)
        {
            _Save = 4;
            try
            {
                btnsua.Visibility = Visibility.Hidden;
                if (fileN == "1") MessageBox.Show("Không có đơn hàng được import");
                else {
                    uscDonHang kq = new uscDonHang(fileN);
                    sPnlContent.Children.Clear();
                    sPnlContent.Children.Add(kq);
                    Data = new DataTable();
                    Data = kq.Data();
                }
                //    dataGrid1.DataContext = XuLiDuLieu.LoadDonHangImport(fileN);
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void mn_DsPc_Click(object sender, RoutedEventArgs e)
        {
           
            _Save = 0;
            //XuLiDuLieu.ThucHienXuLi("insert into donhangnhan values('1','po','ten khach','masap','ten','1/1/2001','1/1/2001','333','33','chuyen may','1/1/2001','1/1/2001','1/1/2001','33','3','3','2/2/2001','1/2/2001','1/1/2011','1','1','1')");
            //PhanCong a = new PhanCong();
            try
            {
                lblStatus.Content = "Đang xử lí dữ liệu xin chờ";
                btnsua.Visibility = Visibility.Hidden;
                if (phancong)
                {
                    phancong = false;
                    //xuongmay.OdbConnect(fileN);
                   
                    XuLiPhanCong.PhanCongDonHang(fileN);
                    
                }
              //  dataGrid1.DataContext = XuLiDuLieu.LoadDonHangNhan();
                uscKetQua kq = new uscKetQua();
                sPnlContent.Children.Clear();
                sPnlContent.Children.Add(kq);
                Data = new DataTable();
                Data = kq.Data();
                lblStatus.Content = "Kết quả phân công đơn đặt hàng";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                try
                {
                    //dataGrid1.DataContext = a.XuLiPhanCong("1").DefaultView;
                }
                catch (Exception ex2)
                {

                }
            }
          
        }

        private void mn_PC_Click(object sender, RoutedEventArgs e)
        {
            _Save = 0;
            try
            {
                btnsua.Visibility = Visibility.Hidden;
                if (fileN != "1")
                {
                    if (phancong)
                    {
                        phancong = false;
                        //xuongmay.OdbConnect(fileN);
                        XuLiPhanCong.PhanCongDonHang(fileN);
                    }
             //       dataGrid1.DataContext = XuLiDuLieu.LoadDonHangNhan();
                    uscKetQua kq = new uscKetQua();
                    sPnlContent.Children.Clear();
                    sPnlContent.Children.Add(kq);
                    Data = new DataTable();
                    Data = kq.Data();
                    lblStatus.Content = "Kết quả phân công đơn đặt hàng";
                }
                else
                {
                    MessageBox.Show("Không có đơn hàng nào import");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            _Save = 0;
            try
            {
                DataTable dt1 = new DataTable();
                dt1 = XuLiDuLieu.LoadDonHangNhan();
              //  dataGrid1.DataContext = dt1.DefaultView;
                uscKetQua kq = new uscKetQua();
                sPnlContent.Children.Clear();
                sPnlContent.Children.Add(kq);
                Data = new DataTable();
                Data = kq.Data();
                lblStatus.Content = "Kết quả phân công đơn đặt hàng";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCheck_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChexkTime ck = new ChexkTime();
                ck.ShowDialog();
            }
            catch (Exception ex) { };
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }
    }
}