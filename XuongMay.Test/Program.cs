﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace XuongMay.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = @"E:\Dropbox\Project\bestschedule\KE HOACH 3 XUONG 16.11.13.xls";
            string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName +
                             ";Extended Properties=\"Excel 12.0;HDR=No;IMEX=1\";";

            var output = new DataSet();

            using (var conn = new OleDbConnection(strConn))
            {
                conn.Open();

                var dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                foreach (DataRow row in dt.Rows)
                {
                    string sheet = row["TABLE_NAME"].ToString();
                    Console.WriteLine(sheet);

                    var cmd = new OleDbCommand("SELECT * FROM [" + sheet + "]", conn);
                    cmd.CommandType = CommandType.Text;

                    OleDbDataAdapter xlAdapter = new OleDbDataAdapter(cmd);

                    //xlAdapter.Fill(output, "School");
                }
                Console.ReadKey(true);
            }
        }
    }
}
